/* 
 * Responsive Styles
 */

<?php $padding = get_option( 'srpg_settings_photo_padding', 5 ) / 2; ?>
 
.photo-thumb {
  float: left;
  padding: <?php echo $padding; ?>px;
}

/* Base/Mobile Styles */

@media (  max-width: <?php echo get_option( 'srpg_settings_medium_breakpoint', 768 ) - 1; ?>px ){
  .srpg-small-cols-1 .photo-thumb {
    clear:both;
    padding-right:0;
  }
  
  .srpg-small-cols-2 .photo-thumb:nth-child(2n + 1) {
    clear: both;
  }
  
  .srpg-small-cols-3 .photo-thumb:nth-child(3n + 1) {
    clear: both;
  }
  
  .srpg-small-cols-4 .photo-thumb:nth-child(4n + 1) {
    clear: both;
  }

  .srpg-small-cols-5 .photo-thumb:nth-child(5n + 1) {
    clear: both;
  }
  
  .srpg-small-cols-6 .photo-thumb:nth-child(6n + 1) {
    clear: both;
  }
  
  .srpg-small-cols-7 .photo-thumb:nth-child(7n + 1) {
    clear: both;
  }
  
  .srpg-small-cols-8 .photo-thumb:nth-child(8n + 1) {
    clear: both;
  }
  
  .srpg-small-cols-9 .photo-thumb:nth-child(9n + 1) {
    clear: both;
  }
  
  .srpg-small-cols-10 .photo-thumb:nth-child(10n + 1) {
    clear: both;
  }
  
  .srpg-small-all {
    width: 100%;
  }

  .srpg-small-1of2 {
    width: 50%;
  }

  .srpg-small-1of3 {
    width: 33.33%;
  }

  .srpg-small-1of4 {
    width: 25%;
  }
  
  .srpg-small-1of5 {
    width: 20%;
  }

  .srpg-small-1of6 {
    width: 16.6666666667%;
  }
  
  .srpg-small-1of7 {
    width: 14.2857142857%;
  }
  
  .srpg-small-1of8 {
    width: 12.5%;
  }
  
  .srpg-small-1of9 {
    width: 11.1111111111%;
  }
  
  .srpg-small-1of10 {
    width: 10%;
  }
}

/* Tablet Styles */

@media ( min-width: <?php echo get_option( 'srpg_settings_medium_breakpoint', 768 ); ?>px ) and ( max-width: <?php echo get_option( 'srpg_settings_large_breakpoint', 1030 ) - 1; ?>px ){
  .srpg-medium-cols-1 .photo-thumb {
    clear:both;
  }
  
  .srpg-medium-cols-2 .photo-thumb:nth-child(2n + 1) {
    clear: both;
  }
  
  .srpg-medium-cols-3 .photo-thumb:nth-child(3n + 1) {
    clear: both;
  }
  
  .srpg-medium-cols-4 .photo-thumb:nth-child(4n + 1) {
    clear: both;
  }

  .srpg-medium-cols-5 .photo-thumb:nth-child(5n + 1) {
    clear: both;
  }
  
  .srpg-medium-cols-6 .photo-thumb:nth-child(6n + 1) {
    clear: both;
  }
  
  .srpg-medium-cols-7 .photo-thumb:nth-child(7n + 1) {
    clear: both;
  }
  
  .srpg-medium-cols-8 .photo-thumb:nth-child(8n + 1) {
    clear: both;
  }
  
  .srpg-medium-cols-9 .photo-thumb:nth-child(9n + 1) {
    clear: both;
  }
  
  .srpg-medium-cols-10 .photo-thumb:nth-child(10n + 1) {
    clear: both;
  }
  
  .srpg-medium-all {
    width: 100%;
  }

  .srpg-medium-1of2 {
    width: 50%;
  }

  .srpg-medium-1of3 {
    width: 33.33%;
  }

  .srpg-medium-1of4 {
    width: 25%;
  }
  
  .srpg-medium-1of5 {
    width: 20%;
  }

  .srpg-medium-1of6 {
    width: 16.6666666667%;
  }
  
  .srpg-medium-1of7 {
    width: 14.2857142857%;
  }
  
  .srpg-medium-1of8 {
    width: 12.5%;
  }
  
  .srpg-medium-1of9 {
    width: 11.1111111111%;
  }
  
  .srpg-medium-1of10 {
    width: 10%;
  }
}

/* Desktop Styles */

@media ( min-width: <?php echo get_option( 'srpg_settings_large_breakpoint', 1030 ); ?>px ) {
  
  .srpg-large-cols-1 .photo-thumb {
    clear:both;
  }
  
  .srpg-large-cols-2 .photo-thumb:nth-child(2n + 1) {
    clear: both;
  }
  
  .srpg-large-cols-3 .photo-thumb:nth-child(3n + 1) {
    clear: both;
  }
  
  .srpg-large-cols-4 .photo-thumb:nth-child(4n + 1) {
    clear: both;
  }

  .srpg-large-cols-5 .photo-thumb:nth-child(5n + 1) {
    clear: both;
  }
  
  .srpg-large-cols-6 .photo-thumb:nth-child(6n + 1) {
    clear: both;
  }
  
  .srpg-large-cols-7 .photo-thumb:nth-child(7n + 1) {
    clear: both;
  }
  
  .srpg-large-cols-8 .photo-thumb:nth-child(8n + 1) {
    clear: both;
  }
  
  .srpg-large-cols-9 .photo-thumb:nth-child(9n + 1) {
    clear: both;
  }
  
  .srpg-large-cols-10 .photo-thumb:nth-child(10n + 1) {
    clear: both;
  }
  
  .srpg-large-all {
    width: 100%;
  }

  .srpg-large-1of2 {
    width: 50%;
  }

  .srpg-large-1of3 {
    width: 33.33%;
  }

  .srpg-large-1of4 {
    width: 25%;
  }
  
  .srpg-large-1of5 {
    width: 20%;
  }

  .srpg-large-1of6 {
    width: 16.6666666667%;
  }
  
  .srpg-large-1of7 {
    width: 14.2857142857%;
  }
  
  .srpg-large-1of8 {
    width: 12.5%;
  }
  
  .srpg-large-1of9 {
    width: 11.1111111111%;
  }
  
  .srpg-large-1of10 {
    width: 10%;
  }  
  
}