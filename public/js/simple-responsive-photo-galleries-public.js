(function( $ ) {
	'use strict';
	
	$(document).ready(function() {
  	$.featherlight.defaults.loading = 'Loading...';
  	
  	$.featherlight.defaults.afterContent = function() {
    	$(".lightbox-caption").remove();
    	var caption = this.$currentTarget.data("caption");
    	
    	if (caption!='') {
    	  var container = $("<div></div>").addClass("lightbox-caption");
        container.text(caption);
        $(".featherlight-content").append(container);
      }
  	}
  	
  	$('a.gallery').featherlightGallery({
        previousIcon: '<span class="dashicons dashicons-controls-back"></span>',
        nextIcon: '<span class="dashicons dashicons-controls-forward"></span>',
        galleryFadeIn: 300,    
        openSpeed: 300
    });
	
	});

})(jQuery);