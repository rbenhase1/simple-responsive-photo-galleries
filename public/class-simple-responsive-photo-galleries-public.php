<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/public
 * @author     Ryan Benhase <rbenhase@2060digital.com>
 */
class Simple_Responsive_Photo_Galleries_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts_styles() {
  	  	
  	if ( is_tax( 'collection' ) || is_post_type_archive( 'gallery' ) || is_singular( 'gallery' ) ) {    
      
      // Only load FeatherLight Gallery Styles and Scripts if Needed
      $gallery_link_thumbnail_to = get_option( 'srpg_settings_gallery_link_thumbnail_to', 'lightbox' );
      $collection_link_thumbnail_to = get_option( 'srpg_settings_collection_link_thumbnail_to', 'gallery' );
      
      if ( $gallery_link_thumbnail_to == 'lightbox' || $collection_link_thumbnail_to == 'lightbox' ) {
        
        wp_enqueue_style( 
    		  'srpg-featherlight-base-style', 
    		  plugin_dir_url( __FILE__ ) . 'css/vendor/featherlight.min.css',
    		  array(), 
    		  $this->version, 
    		  'all' 
    		);
    		
        wp_enqueue_style( 
    		  'srpg-featherlight-gallery-style', 
    		  plugin_dir_url( __FILE__ ) . 'css/vendor/featherlight.gallery.min.css',
    		  array(), 
    		  $this->version, 
    		  'all' 
    		);
    		
        wp_enqueue_script( 
    		  'srpg-featherlight-base', 
    		  plugin_dir_url( __FILE__ ) . 'js/vendor/featherlight.min.js',
    		  array( 'jquery' ), 
    		  $this->version, 
    		  'all' 
    		);
    		
        wp_enqueue_script( 
    		  'srpg-featherlight-gallery', 
    		  plugin_dir_url( __FILE__ ) . 'js/vendor/featherlight.gallery.min.js',
    		  array( 'jquery', 'srpg-featherlight-base' ), 
    		  $this->version, 
    		  'all' 
    		);
    		
        wp_enqueue_script( 
    		  $this->plugin_name . '-public', 
    		  plugin_dir_url( __FILE__ ) . 'js/simple-responsive-photo-galleries-public.js',
    		  array( 'jquery', 'srpg-featherlight-base', 'srpg-featherlight-gallery' ), 
    		  $this->version, 
    		  'all' 
    		);
  		
      }    
      
      // Enqueue base public styles
  		wp_enqueue_style( 
  		  $this->plugin_name, 
  		  plugin_dir_url( __FILE__ ) . 'css/simple-responsive-photo-galleries-public.css',
  		  array(), 
  		  $this->version, 
  		  'all' 
  		);
  		
  		// Enqueue dynamic styles (which are affected by settings on the options page)
  		wp_enqueue_style(
  		  'srpg-dynamic-css',
         admin_url('admin-ajax.php').'?action=srpg_dynamic_css',
         array(),
         $this->version,
         'all'
      );
    }
	}

	
	
	/**
	 * Generate a dynamic stylesheet based on options on the settings page.
	 * 
	 * @access public
	 * @return void
	 */
	public function dynamic_stylesheet() {
  	header('Content-type: text/css');
    ob_start();
    require_once( 'css/simple-responsive-photo-galleries-dynamic.css.php' );
    $css = ob_get_contents();
    ob_end_clean();
    echo $css; 
    exit;
	}
	
	/**
	 * Gets the classes to attach to the container element
	 * in order to specify the number of rows. 
	 * 
	 * @access public
	 * @param String $context The context for which to retrieve options ('collection' or 'gallery').
	 * @return String $container_classes The classes, separated by spaces
	 */
	public function get_container_classes( $context ) {
  	  	
  	$container_classes = '';
    	
  	$cols_small = get_option( 'srpg_settings_' . $context . '_cols_small', 1 );  	
    $container_classes .= " srpg-small-cols-" . $cols_small;
    
  	$cols_medium = get_option( 'srpg_settings_' . $context . '_cols_medium', 3 );  	
    $container_classes .= " srpg-medium-cols-" . $cols_medium;
    
  	$cols_large = get_option( 'srpg_settings_' . $context . '_cols_large', 5 );  	
    $container_classes .= " srpg-large-cols-" . $cols_large;

    return $container_classes;
	}	

	/**
	 * Gets the classes to attach to individual photo container elements
	 * for displaying them in a responsive layout.
	 * 
	 * @access public
	 * @param String $context The context for which to retrieve options ('collection' or 'gallery').
	 * @return String $layout_classes The classes, separated by spaces
	 */
	public function get_layout_classes( $context ) {
  	  	
  	$layout_classes = '';
    	
  	$cols_small = get_option( 'srpg_settings_' . $context . '_cols_small', 1 );  	
  	
  	if ( $cols_small == 1 ) {
  	  $layout_classes .= " srpg-small-all";
    } else {
      $layout_classes .= " srpg-small-1of" . $cols_small  ;
    }
    
  	$cols_medium = get_option( 'srpg_settings_' . $context . '_cols_medium', 3 );  
  	
  	if ( $cols_medium == 1 ) {
  	  $layout_classes .= " srpg-medium-all";
    } else {
      $layout_classes .= " srpg-medium-1of" . $cols_medium;
    }
    
  	$cols_large = get_option( 'srpg_settings_' . $context . '_cols_large', 5 );  	
  	
  	if ( $cols_large == 1 ) {
  	  $layout_classes .= " srpg-large-all";
    } else {
      $layout_classes .= " srpg-large-1of" . $cols_large;
    }
    
    return $layout_classes;
	}
	
	
	/**
	 * Trim the number of photos to be displayed based on settings.
	 * 
	 * @access public
	 * @param Array $photos The photos attached to a gallery
	 * @return Array $photos The trimmed array of photos
	 */
	public function trim_photos_for_collection( $photos ) {

  	$limit = get_option( 'srpg_settings_collection_photo_limit', -1 );
  	
  	if ( $limit == -1 )
  	  return $photos;
  	  
    if ( $limit == 0 ) 
      return array();
  	
  	// Return first N items of array, where N = $limit
  	return array_slice( $photos, 0, $limit );
	}
	
	
	
	public function get_thumbnail_link( $attachment_id, $link_to ) {
  	
  	global $post;
  	
  	switch( $link_to ) {
    	case 'gallery':
    	  return '<a href="' . get_permalink( $post->ID ) . '">';
    	  break;
    	  
    	case 'lightbox':
    	  
    	  $caption = get_post_meta( $attachment_id, 'photo-caption', true );
    	  
    	  $url = wp_get_attachment_image_src( $attachment_id, 'full' );
    	  return '<a class="gallery" data-caption="' . esc_attr( $caption ) . '" href="' . $url[0] . '">';
    	  break;
    	  
    	case 'raw':
    	  $url = wp_get_attachment_image_src( $attachment_id, 'full' );
    	  return '<a target="_blank" href="' . $url[0] . '">';
    	  break;
    	  
    	case 'attachment':
    	  return '<a href="' . get_permalink( $attachment_id ) . '">';
    	  break;
    	  
      default: 
        return '<a href="#">';
  	}
	}
	
	
	/**
	 * Get the HTML output for a single photo thumbnail in a gallery/collection.
	 * 
	 * @access public
	 * @param Int $attachment_id The ID of the attachment
	 * @param String $layout_classes The layout classes to include in the container
	 * @param String $context The context in which this is being displayed (i.e. 'gallery' or 'collection')
	 * @return String $html The HTML output to render
	 */
	public function get_single_photo_thumb( $attachment_id, $layout_classes, $context ) {
  	
    $caption = get_post_meta( $attachment_id, 'photo-caption', true );          
    $render_caption = get_option( 'srpg_settings_' . $context . '_render_caption', false );
    
    $src_thumb = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );         
    $src_full = wp_get_attachment_image_src( $attachment_id, 'full' );      
    
    if ( $context == 'gallery' ) {
      $link_to = get_option( 'srpg_settings_gallery_link_thumbnail_to', 'lightbox' );
    } else if ( $context == 'collection' ) {
      $link_to = get_option( 'srpg_settings_collection_link_thumbnail_to', 'gallery' );
    }

    $html = '<div class="photo-thumb ' . $layout_classes . '">';        
    $html .= $this->get_thumbnail_link( $attachment_id, $link_to );    
  	$html .= '<img src="' . $src_thumb[0] . '" alt="Photo" title="' . esc_attr( $caption ) . '">';
  	
  	if ( !empty( $caption ) && ( $render_caption ) )
  	  $html .= '<div class="photo-caption">' . esc_html( $caption ) . '</div>';
  	  
  	$html .= '</a>';  	     
  	$html .= '</div>';      	
  	
  	return $html;
	}
	
	
	/**
	 * Filters the_content to add gallery / gallery preview
	 * 
	 * @access public
	 * @param String $content The original post content (i.e. body)
	 * @return String $content The filtered content, including gallery
	 */
	public function display_gallery( $content ) {
  	
    $gallery_link_thumbnail_to = get_option( 'srpg_settings_gallery_link_thumbnail_to', 'lightbox' );
    $collection_link_thumbnail_to = get_option( 'srpg_settings_collection_link_thumbnail_to', 'gallery' );
  	  	
  	if ( is_tax( 'collection' ) || is_post_type_archive( 'gallery' ) ) {  
    	  
      /* 
       * Collection Archive or Gallery Archive  
       */      	    	
       
    	global $post;
      $photos = get_post_meta( $post->ID, 'srpg_photos', true );     
      $container_classes = $this->get_container_classes( 'collection' ); 	 	
      
      // List Galleries Based on Preview Type Option 
      $collection_display_type = get_option( 'srpg_settings_collection_display_type', 'single' );
      
      /* If we are displaying a selction of gallery photo thumbnails as a preview */
      if ( $collection_display_type == 'multi' ) {
        
        // Determine whether to render the gallery post body content     
        $render_body = get_option( 'srpg_settings_collection_render_body', false );
        
        if ( $render_body ) {
          $content .= '<div class="srpg_section srpg_photos_container srpg_archive' . $container_classes . '">';
        } else {
          $content = '<div class="srpg_section srpg_photos_container srpg_archive' . $container_classes . '">';
        }
        
        if ( !empty( $photos ) ) {  
                     
          $layout_classes = $this->get_layout_classes( 'collection' );        
          $photos = $this->trim_photos_for_collection( $photos ); 
                      
          foreach( $photos as $attachment_id ) {         
            $content .=  $this->get_single_photo_thumb( $attachment_id, $layout_classes, 'collection' );          
          }
        }
        $content .= '</div>';    

      /* If we are displaying a single thumbnail as a preview */
      } else if ( $collection_display_type == 'single' ) {      
        $content = '<div class="srpg_section srpg_single_gallery_preview srpg_archive">';
        
        $attachment_id = reset( $photos );
        $src_thumb = wp_get_attachment_image_src( $attachment_id, 'thumbnail' ); 
        
        $content .=  '<a href="' . get_permalink( $post->ID ) . '">';
        $content .= '<img src="' . $src_thumb[0] . '" alt="Photo Gallery" title="' . esc_attr( $post->post_title ) . '">';
        $content .= '</a>';  
        $content .= '</div>';
      }
	
  	} else if ( is_singular( 'gallery' ) ){
    	
      /* 
       * Single Gallery View 
       */  
        	
    	global $post;
      $photos = get_post_meta( $post->ID, 'srpg_photos', true );     
      $container_classes = $this->get_container_classes( 'gallery' ); 	 	
        
      $content .= '<div class="srpg_section srpg_photos_container srpg_single_gallery' . $container_classes . '">';
      
      if ( !empty( $photos ) ) {   
        
        $layout_classes = $this->get_layout_classes( 'gallery' );        
           
        foreach( $photos as $attachment_id ) {         
          $content .= $this->get_single_photo_thumb( $attachment_id, $layout_classes, 'gallery' );          
        }      
      }
      $content .= '</div>';
	  }    
    return $content;
	}		

}
