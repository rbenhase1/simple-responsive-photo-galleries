<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/includes
 * @author     Ryan Benhase <rbenhase@2060digital.com>
 */
class Simple_Responsive_Photo_Galleries {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Simple_Responsive_Photo_Galleries_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'simple-responsive-photo-galleries';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_shared_hooks();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Simple_Responsive_Photo_Galleries_Loader. Orchestrates the hooks of the plugin.
	 * - Simple_Responsive_Photo_Galleries_i18n. Defines internationalization functionality.
	 * - Simple_Responsive_Photo_Galleries_Admin. Defines all hooks for the admin area.
	 * - Simple_Responsive_Photo_Galleries_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-simple-responsive-photo-galleries-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-simple-responsive-photo-galleries-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-simple-responsive-photo-galleries-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-simple-responsive-photo-galleries-public.php';

		$this->loader = new Simple_Responsive_Photo_Galleries_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Simple_Responsive_Photo_Galleries_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Simple_Responsive_Photo_Galleries_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}
	
	/**
	 * Register all of the hooks which are shared between the admin and public facing sides of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_shared_hooks() {

    $this->loader->add_action( 'init', $this, 'register_post_types_and_taxonomies', 0 );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Simple_Responsive_Photo_Galleries_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'add_meta_boxes_gallery', $plugin_admin, 'add_meta_boxes' );
		$this->loader->add_action( 'save_post', $plugin_admin, 'save_meta_boxes' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_admin_menu' );
    $this->loader->add_action( 'admin_init', $plugin_admin, 'settings_init' );
    $this->loader->add_action( 'wp_ajax_srpg_update_caption', $plugin_admin, 'update_caption_ajax' );
    $this->loader->add_action( 'wp_ajax_srpg_get_caption', $plugin_admin, 'get_caption_ajax' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Simple_Responsive_Photo_Galleries_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts_styles' );
		$this->loader->add_filter( 'the_content', $plugin_public, 'display_gallery' );
		
		$this->loader->add_action( 'wp_ajax_srpg_dynamic_css', $plugin_public, 'dynamic_stylesheet' );
		$this->loader->add_action( 'wp_ajax_nopriv_srpg_dynamic_css', $plugin_public, 'dynamic_stylesheet' );

	}
	
	/**
	 * Register the Photo Gallery post type, as well as the "Collection" taxonomy.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	public function register_post_types_and_taxonomies() {
  	
    $labels = array(
  		'name'                => _x( 'Photo Galleries', 'Post Type General Name', 'simple-responsive-photo-galleries' ),
  		'singular_name'       => _x( 'Photo Gallery', 'Post Type Singular Name', 'simple-responsive-photo-galleries' ),
  		'menu_name'           => __( 'Photos', 'simple-responsive-photo-galleries' ),
  		'name_admin_bar'      => __( 'Photo Gallery', 'simple-responsive-photo-galleries' ),
  		'parent_item_colon'   => __( 'Parent Item:', 'simple-responsive-photo-galleries' ),
  		'all_items'           => __( 'All Galleries', 'simple-responsive-photo-galleries' ),
  		'add_new_item'        => __( 'Add New Gallery', 'simple-responsive-photo-galleries' ),
  		'add_new'             => __( 'Add New', 'simple-responsive-photo-galleries' ),
  		'new_item'            => __( 'New Gallery', 'simple-responsive-photo-galleries' ),
  		'edit_item'           => __( 'Edit Gallery', 'simple-responsive-photo-galleries' ),
  		'update_item'         => __( 'Update Gallery', 'simple-responsive-photo-galleries' ),
  		'view_item'           => __( 'View Gallery', 'simple-responsive-photo-galleries' ),
  		'search_items'        => __( 'Search Gallery', 'simple-responsive-photo-galleries' ),
  		'not_found'           => __( 'Not found', 'simple-responsive-photo-galleries' ),
  		'not_found_in_trash'  => __( 'Not found in Trash', 'simple-responsive-photo-galleries' ),
  	);
  	
  	$rewrite = array(
  		'slug'                => 'photos',
  		'with_front'          => true,
  		'pages'               => true,
  		'feeds'               => true,
  	);
  	
  	$args = array(
  		'label'               => __( 'Photo Gallery', 'simple-responsive-photo-galleries' ),
  		'description'         => __( 'A gallery of photos', 'simple-responsive-photo-galleries' ),
  		'labels'              => $labels,
  		'supports'            => array( 'title', 'editor', ),
  		'taxonomies'          => array( 'collection' ),
  		'hierarchical'        => false,
  		'public'              => true,
  		'show_ui'             => true,
  		'show_in_menu'        => true,
  		'menu_position'       => 5,
  		'show_in_admin_bar'   => true,
  		'show_in_nav_menus'   => true,
  		'can_export'          => true,
  		'has_archive'         => true,	
  		'menu_icon'           => 'dashicons-format-gallery',	
  		'exclude_from_search' => false,
  		'publicly_queryable'  => true,
  		'rewrite'             => $rewrite,
  		'capability_type'     => 'page',
  	);
  	register_post_type( 'gallery', $args );
  	
    $labels = array(
  		'name'                       => _x( 'Collections', 'Taxonomy General Name', 'simple-responsive-photo-galleries' ),
  		'singular_name'              => _x( 'Collection', 'Taxonomy Singular Name', 'simple-responsive-photo-galleries' ),
  		'menu_name'                  => __( 'Collections', 'simple-responsive-photo-galleries' ),
  		'all_items'                  => __( 'All Collections', 'simple-responsive-photo-galleries' ),
  		'parent_item'                => __( 'Parent Collection', 'simple-responsive-photo-galleries' ),
  		'parent_item_colon'          => __( 'Parent Collection:', 'simple-responsive-photo-galleries' ),
  		'new_item_name'              => __( 'New Collection', 'simple-responsive-photo-galleries' ),
  		'add_new_item'               => __( 'Add New Collection', 'simple-responsive-photo-galleries' ),
  		'edit_item'                  => __( 'Edit Collection', 'simple-responsive-photo-galleries' ),
  		'update_item'                => __( 'Update Collection', 'simple-responsive-photo-galleries' ),
  		'view_item'                  => __( 'View Collection', 'simple-responsive-photo-galleries' ),
  		'separate_items_with_commas' => __( 'Collections are groups of galleries; each gallery can be added to multiple collections, and each collection can contain multiple galleries. Separate collections with commas.', 'simple-responsive-photo-galleries' ),
  		'add_or_remove_items'        => __( 'Add or remove collections', 'simple-responsive-photo-galleries' ),
  		'choose_from_most_used'      => __( 'Choose from the most used', 'simple-responsive-photo-galleries' ),
  		'popular_items'              => __( 'Popular Collections', 'simple-responsive-photo-galleries' ),
  		'search_items'               => __( 'Search Collection', 'simple-responsive-photo-galleries' ),
  		'not_found'                  => __( 'Not Found', 'simple-responsive-photo-galleries' ),
  	);
  	$args = array(
  		'labels'                     => $labels,
  		'hierarchical'               => true,
  		'public'                     => true,
  		'show_ui'                    => true,
  		'show_admin_column'          => true,
  		'show_in_nav_menus'          => true,
  		'show_tagcloud'              => true,
  	);
  	register_taxonomy( 'collection', array( 'gallery' ), $args );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Simple_Responsive_Photo_Galleries_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
