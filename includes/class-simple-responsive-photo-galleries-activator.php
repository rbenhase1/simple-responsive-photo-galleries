<?php

/**
 * Fired during plugin activation
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/includes
 * @author     Ryan Benhase <rbenhase@2060digital.com>
 */
class Simple_Responsive_Photo_Galleries_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
     flush_rewrite_rules();
	}

}
