<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/includes
 * @author     Ryan Benhase <rbenhase@2060digital.com>
 */
class Simple_Responsive_Photo_Galleries_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
