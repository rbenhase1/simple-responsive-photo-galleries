# Simple Responsive Photo Galleries #
Contributors: rbenhase
Donate link: http://ryanbenhase.com
Tags: photo galleries, photos, photo albums
Requires at least: 3.8
Tested up to: 4.3
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin allows for the easy creation of responsive photo galleries, but without being bloated (as many other plugins are).

## Description ##

The Simple Responsive Photo Galleries plugin will allow you to create simple photo galleries with ease. It is unlike 
many other photo gallery plugins in that it:

*   Uses a custom post type for galleries and a WP media library attachment for each photo, allowing galleries to be 
    queried like any other post type and customized with theme templates. 
*   Offers "collections" as a custom taxonomy (which works just like post categories) instead of some awkward and 
    un-intuitive interface for grouping galleries into "albums."
*   Allows you to set breakpoints (in pixels) between small, medium, and large layouts (e.g., for mobile, tablet, and desktop).
*   Allows you to choose the number of columns for your galleries/collections based on these layout sizes (breakpoints).
*   Isn't incredibly feature-rich, but also isn't as bloated or clunky as many other plugins. 

    It should be noted that this plugin is not for everyone, but for some uses, it blows the competition out of the water.

## Installation ##

This section describes how to install the plugin and get it working.

1. Upload the `simple-responsive-photo-galleries` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Visit Settings > Simple Responsive Photo Galleries to configure the plugin.

## Frequently Asked Questions ##

### Where is my gallery archive or collection archive? ###

Your main gallery archive will appear at yoursite.com/photos, where *yoursite.com* is your actual domain name. If it does not appear here, check to 
ensure that you do not have a page or post using `/photos` as the permalink, as this will cause conflicts. 

Your collection archives will appear at yoursite.com/collection/collection-name, where *yoursite.com* is your actual domain name and *collection-name* is 
the slug for your collection. You can also find this in the Wordpress admin under Photos > Collections; just find your collection, hover over it, and
click the 'View' link.

### How can I re-order the photos in a gallery? ###
Once you add photos to a gallery, you can drag-and-drop the thumbnails on the 'Edit Gallery' screen to reorder them.

### How can I add a caption to a photo? ###
To add a caption, select the photo by clicking its thumbnail on the 'Edit Gallery' screen. A pencil icon should then appear; click that, and you will
be able to enter your caption in the popup dialog.

### How can I remove a photo from a gallery? ###
To remove a photo, select the photo by clicking its thumbnail on the 'Edit Gallery' screen. A pencil icon should then appear; click that, and you will
be able to click the "Delete Photo" button. You will then need to save/update your gallery in order for the change to take effect. Also note that this 
simply removes your photo from your gallery; it does not delete file itself, nor remove it from your Wordpress media library. 

### How can I have my photos open in a lightbox? ###
Visit Settings > Simple Responsive Photo Galleries and make sure that the "Link Thumbnails To" setting has "Lightbox View of Full Image" selected. Note
that this setting for a gallery is separate from the setting for a collection/archive.

### How can I have my collection or archive page show only one preview thumbnail for each gallery, which then clicks through to the whole gallery? ###
Under Settings > Simple Responsive Photo Galleries, find the "Collection/Archive Settings" section and set the "Gallery Preview Type In Collections/Archives" 
field to "Single Photo Thumbnail Linked to Gallery."

If you want to customize how this looks on the front end of your site (e.g., to have the title of each gallery appear beneath each preview thumbnail, or 
to put each gallery into its own column on the collection/archive pages), you will need to adjust your theme template/styles accordingly. This plugin does 
not control how your individual archive posts are displayed, only how photos within a gallery are displayed.

It's suggested that you create separate templates in your theme specifically for galleries and collections (e.g. archive-gallery.php and taxonomy-collection.php) 
so you can avoid making changes that will affect your other post types.

### I don't like how plain my galleries look. What can I do about that? ###
This plugin is intended to be as bare-bones as possible, and so, aside from the layout, very little attention has been given to styling. You can 
(and should) use your Wordpress theme to style your galleries as you see fit; it's generally good to keep style/presentation in the theme layer, anyway, 
and let plugins focus on functionality.

## Troubleshooting ##

### My gallery, collection, or gallery archive does not display correctly on my site/theme ###

Assuming that you have already gone to Settings > Simple Responsive Photo Galleries to configure the plugin, the problem is probably related to your theme.

This plugin filters `the_content` and thus requires your relevant theme templates to call `<?php the_content(); ?>` somewhere in the code. 
This should be the first thing you check, especially if your problem is with your archive or collection pages, since these sort of 
page templates will sometimes only render the excerpt (i.e. using `<?php the_excerpt(); ?>`) and not the full post content. You can create 
separate templates in your theme specifically for galleries and collections (e.g. single-gallery.php, archive-gallery.php, taxonomy-collection.php) 
if you want to avoid making changes that will affect your other post types; just make sure `<?php the_content(); ?>` is included somewhere inside
the Wordpress loop.

If that doesn't fix things, check to ensure that there aren't any conflicts with other plugins (e.g. anything else that might filter `the_content` or 
create a custom post type called 'gallery').

### My gallery, collection, or gallery archive looks funky (and not in a good George Clinton way) ###
Your theme stylesheet may be affecting the gallery in an adverse way; you will need to adjust your theme stylesheet accordingly. Unfortunately, it's not 
possible to provide support for every possible problem here, but there should be enough classes included in your gallery HTML elements to give you the 
selectors needed to override any styling issues you might encounter (e.g. directives that target `.srpg_section` or `.photo-thumb`).

### I get a 404 Page Not Found error when I try to view a gallery/collection/archive ###

You may need to manually flush your rewrite rules. The simplest way to do this is to visit Settings > Permalinks in your Wordpress backend, and 
then click the "Save Changes" button. 

### My Lightbox Isn't Working ###
First, visit Settings > Simple Responsive Photo Galleries and make sure that the "Link Thumbnails To" setting has "Lightbox View of Full Image" selected in
the appropriate section ("Gallery Settings" for individual galleries, and "Collection/Archive Settings" for collection/archive pages). 

If this is set correctly and the Lightbox still isn't working, make sure that you do not have any Javascript errors on the page where your 
gallery/collection/archive is being displayed; these can interfere with the Lightbox functionality. 

Also make sure that you're not just being impatient; if your photos are huge, the Lightbox will probably take some time to load.

### My Lightbox Takes Forever to Load ###
Your photo files are probably huge, which is a very bad thing. Either resize your photos before uploading them to your site, or else use a plugin like Imsanity, which 
will resize any giant photos uploaded to your site to a more reasonable size. 


## Screenshots ##

1. The options page
2. The 'Add New Gallery' page

## Changelog ##

### 1.0 ###
* Initial plugin release