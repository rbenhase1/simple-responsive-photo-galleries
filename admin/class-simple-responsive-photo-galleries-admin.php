<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://ryanbenhase.com
 * @since      1.0.0
 *
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Simple_Responsive_Photo_Galleries
 * @subpackage Simple_Responsive_Photo_Galleries/admin
 * @author     Ryan Benhase <rbenhase@2060digital.com>
 */
class Simple_Responsive_Photo_Galleries_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( 
		  $this->plugin_name, 
		  plugin_dir_url( __FILE__ ) . 'css/simple-responsive-photo-galleries-admin.css',
		  array(), 
		  $this->version, 
		  'all' 
		);
		
		wp_enqueue_style( 
		  'srpg-jquery-ui-base',
		  plugin_dir_url( __FILE__ ) . 'css/jquery-ui/jquery-ui.min.css',
		  array(), 
		  $this->version, 
		  'all' 
		);
		
    wp_enqueue_style( 
		  'srpg-jquery-ui-structure',
		  plugin_dir_url( __FILE__ ) . 'css/jquery-ui/jquery-ui.structure.min.css',
		  array( 'srpg-jquery-ui-base' ), 
		  $this->version, 
		  'all' 
		);
		
    wp_enqueue_style( 
		  'srpg-jquery-ui-theme',
		  plugin_dir_url( __FILE__ ) . 'css/jquery-ui/jquery-ui.theme.min.css',
		  array( 'srpg-jquery-ui-base', 'srpg-jquery-ui-structure' ), 
		  $this->version, 
		  'all' 
		);
		

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		
		global $current_screen;
		
		// Add/Edit Gallery Page
		if ( $current_screen->post_type == 'gallery' ) {
      wp_enqueue_media();

      // Registers and enqueues the edit gallery page javascript.
      wp_register_script( 
        'srpg-gallery-admin', 
        plugin_dir_url( __FILE__ ) . 'js/srpg-gallery-admin.js', 
        array( 'jquery', 'jquery-ui-dialog', 'jquery-ui-sortable' ) 
      );
      
      wp_localize_script( 'srpg-gallery-admin', 'srpg_photos',
        array(
            'title' => 'Choose or Upload Gallery Photos',
            'button' => 'Select Photos',
        )
      );
      
      wp_enqueue_script( 'srpg-gallery-admin' );
    
    // Settings Page
    } else if ( $current_screen->id == 'settings_page_simple_responsive_photo_galleries' ) {
      // Enqueues the settings page javascript.
      wp_enqueue_script( 
        'srpg-settings', 
        plugin_dir_url( __FILE__ ) . 'js/srpg-settings.js', 
        array( 'jquery' ) 
      );
    }
	}
	
	
	/**
	 * Adds meta boxes to the photo gallery post type.
	 * 
	 * @access public
	 * @return void
	 */
	public function add_meta_boxes() {
    add_meta_box(
    	'srpg_photos',
    	__( 'Photos', $this->plugin_name ),
    	array( $this, 'photos_meta_box' ),
    	'gallery',
    	'normal',
    	'high'
    );
	}
	
	/**
   * Renders the photos meta box on gallery type
   * 
   * @param WP_Post $post The object for the current post/page.
   */
  public function photos_meta_box( $post ) {
  
    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'srpg_meta', 'srpg_meta_nonce' );
  
    $photos = get_post_meta( $post->ID, 'srpg_photos', true );    
    
    echo '<small>Upload your photos here; you will then be able to drag-and-drop to re-order them. <br>To add a caption or delete a photo, click on the <span class="dashicons dashicons-edit"></span> icon which appears when you select a photo.</small>';
    echo '<p><input type="button" id="meta-photo-button" class="button" value="Choose or Upload Photos"></p>';    
    
    echo '<ul id="srpg_photos_container">';
    
    if ( !empty( $photos ) ) {   
         
      foreach( $photos as $attachment_id ) {
        
        $caption = get_post_meta( $attachment_id, 'photo-caption', true );   
        
        $src = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );         
        $src_medium = wp_get_attachment_image_src( $attachment_id, 'medium' );      

        echo '<li class="photo-preview" data-attachment="' . $attachment_id . '">';
        
        echo '<img src="' . $src[0] . '" alt="Photo Preview" title="' . esc_attr( $caption ) . '">';
             
        echo '<input type="hidden" name="srpg_photos[]" value="' . $attachment_id . '">';
        echo '<a href="#" data-medium-url="' . $src_medium[0] . '" data-attachment="' . $attachment_id . 
             '" data-caption="' . esc_attr( $caption ) . '" class="edit-photo">' . 
             '<span class="dashicons dashicons-edit"></span></a>';
        echo '</li>';        
      }      
    }
    echo '</ul>';
    echo '<div class="clearfix"></div>';

  }

  
  /**
   * Saves meta boxes along with post
   *
   * @param int $post_id The ID of the post being saved.
   * @return void
   */
  public function save_meta_boxes( $post_id ) {
    
    // Check if our nonce is set.
    if ( ! isset( $_POST['srpg_meta_nonce'] ) ) {
    	return;
    }
  
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['srpg_meta_nonce'], 'srpg_meta' ) ) {
    	return;
    }
  
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    	return;
    }
  
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}		
		    
    if ( isset( $_POST['srpg_photos'] ) ) {
    	update_post_meta( $post_id, 'srpg_photos', $_POST['srpg_photos'] );
    } 
    
  }
  
  
  /**
   * AJAX fuction to update a photo caption;
   * echoes a JSON-encoded response.
   * 
   * @access public
   * @return void
   */
  public function update_caption_ajax() {
    
    header( 'Content-Type: application/json' );
    
    if ( isset( $_POST['attachment'] ) ) {
      
      $result = update_post_meta( 
        $_POST['attachment'], 
        'photo-caption', 
        ( isset( $_POST['caption'] ) ? $_POST['caption'] : '' ) 
      );
    }
    
    echo json_encode( array( 'success' => ( !empty( $result ) ) ) );
    exit;
  }

  /**
   * AJAX fuction to get a photo caption;
   * echoes a JSON-encoded response.
   * 
   * @access public
   * @return void
   */
  public function get_caption_ajax() {
    
    header( 'Content-Type: application/json' );
    
    if ( isset( $_POST['attachment'] ) ) {
      
      $caption =get_post_meta( 
        $_POST['attachment'], 
        'photo-caption', 
        true 
      );
    }
    
    echo json_encode( array( 'caption' => ( !empty( $caption ) ? $caption : '' ) ) );
    exit;
  }
	
	
	/**
	 * Add the settings page to the admin menu.
	 * 
	 * @access public
	 * @return void
	 */
	public function add_admin_menu(  ) { 

    add_options_page( 
      'Simple Responsive Photo Galleries', 
      'Simple Responsive Photo Galleries', 
      'manage_options', 
      'simple_responsive_photo_galleries', 
      array( $this, 'options_page' ) 
    );  
  }
  
  
	/**
	 * Initialize Settings page.
	 * 
	 * @access public
	 * @return void
	 */
	public function settings_init() { 
    
    /* Register all Settings */
    register_setting( 'settingsPage', 'srpg_settings_medium_breakpoint', 'intval' );
    register_setting( 'settingsPage', 'srpg_settings_large_breakpoint', 'intval' );
    register_setting( 'settingsPage', 'srpg_settings_photo_padding', 'intval' );
    
    register_setting( 'settingsPage', 'srpg_settings_gallery_cols_small', 'intval' );
    register_setting( 'settingsPage', 'srpg_settings_gallery_cols_medium', 'intval' );
    register_setting( 'settingsPage', 'srpg_settings_gallery_cols_large', 'intval' );
    register_setting( 'settingsPage', 'srpg_settings_gallery_render_caption' );
    register_setting( 'settingsPage', 'srpg_settings_gallery_link_thumbnail_to' );
     
    register_setting( 'settingsPage', 'srpg_settings_collection_cols_small', 'intval' );
    register_setting( 'settingsPage', 'srpg_settings_collection_cols_medium', 'intval' );
    register_setting( 'settingsPage', 'srpg_settings_collection_cols_large', 'intval' );   
    register_setting( 'settingsPage', 'srpg_settings_collection_display_type' );       
    register_setting( 'settingsPage', 'srpg_settings_collection_photo_limit', 'intval' );
    register_setting( 'settingsPage', 'srpg_settings_collection_render_body' );
    register_setting( 'settingsPage', 'srpg_settings_collection_render_caption' );
    register_setting( 'settingsPage', 'srpg_settings_collection_link_thumbnail_to' );
    
    
    /* Breakpoint Settings */
    add_settings_section(
    	'srpg_settingsPage_breakpoint_section', 
    	__( '<hr>Responsive Layout Settings', $this->plugin_name ), 
    	array( $this, 'settings_section_breakpoint_callback' ), 
    	'settingsPage'
    );
    
    add_settings_field( 
    	'medium_breakpoint', 
    	__( 'Minimum Width - Medium Breakpoint (in pixels)', $this->plugin_name ), 
    	array( $this, 'render_field_medium_breakpoint' ), 
    	'settingsPage', 
    	'srpg_settingsPage_breakpoint_section' 
    );
    
    add_settings_field( 
    	'large_breakpoint', 
    	__( 'Minimum Width - Large Breakpoint (in pixels)', $this->plugin_name ), 
    	array( $this, 'render_field_large_breakpoint' ), 
    	'settingsPage', 
    	'srpg_settingsPage_breakpoint_section' 
    );
    
    add_settings_field( 
    	'photo_padding', 
    	__( 'Minimum Distance Between Photos <br>(in pixels)', $this->plugin_name ), 
    	array( $this, 'render_field_photo_padding' ), 
    	'settingsPage', 
    	'srpg_settingsPage_breakpoint_section' 
    );
    

    /* Gallery Settings */
    add_settings_section(
    	'srpg_settingsPage_gallery_section', 
    	__( '<hr>Gallery Settings', $this->plugin_name ), 
    	array( $this, 'settings_section_gallery_callback' ), 
    	'settingsPage'
    );
  
    add_settings_field( 
    	'gallery_cols_small', 
    	__( 'Columns - Small Size', $this->plugin_name ), 
    	array( $this, 'render_field_gallery_cols_small' ), 
    	'settingsPage', 
    	'srpg_settingsPage_gallery_section' 
    );
    
    add_settings_field( 
    	'gallery_cols_medium', 
    	__( 'Columns - Medium Size', $this->plugin_name ), 
    	array( $this, 'render_field_gallery_cols_medium' ), 
    	'settingsPage', 
    	'srpg_settingsPage_gallery_section' 
    );
    
    add_settings_field( 
    	'gallery_cols_large', 
    	__( 'Columns - Large Size', $this->plugin_name ), 
    	array( $this, 'render_field_gallery_cols_large' ), 
    	'settingsPage', 
    	'srpg_settingsPage_gallery_section' 
    );
    
    add_settings_field( 
    	'gallery_render_caption', 
    	__( 'Photo Captions on Gallery Pages', $this->plugin_name ), 
    	array( $this, 'render_field_gallery_caption' ), 
    	'settingsPage', 
    	'srpg_settingsPage_gallery_section' 
    );

    add_settings_field( 
    	'gallery_link_thumbnail_to', 
    	__( 'Link Thumbnails To', $this->plugin_name ), 
    	array( $this, 'render_field_gallery_link_thumbnail_to' ), 
    	'settingsPage', 
    	'srpg_settingsPage_gallery_section' 
    );
    
    
    /* Collection Settings */
    
    add_settings_field( 
    	'collection_display_type', 
    	__( 'Gallery Preview Type<br>In Collections/Archives', $this->plugin_name ), 
    	array( $this, 'render_field_collection_display_type' ), 
    	'settingsPage', 
    	'srpg_settingsPage_collection_section' 
    );
    
    add_settings_section(
    	'srpg_settingsPage_collection_section', 
    	__( '<hr>Collection/Archive Settings', $this->plugin_name ), 
    	array( $this, 'settings_section_collection_callback' ), 
    	'settingsPage'
    );
  
    add_settings_field( 
    	'collection_cols_small', 
    	__( 'Gallery Preview Columns - Small Size', $this->plugin_name ), 
    	array( $this, 'render_field_collection_cols_small' ), 
    	'settingsPage', 
    	'srpg_settingsPage_collection_section' 
    );
    
    add_settings_field( 
    	'collection_cols_medium', 
    	__( 'Gallery Preview Columns - Medium Size', $this->plugin_name ), 
    	array( $this, 'render_field_collection_cols_medium' ), 
    	'settingsPage', 
    	'srpg_settingsPage_collection_section' 
    );
    
    add_settings_field( 
    	'collection_cols_large', 
    	__( 'Gallery Preview Columns - Large Size', $this->plugin_name ), 
    	array( $this, 'render_field_collection_cols_large' ), 
    	'settingsPage', 
    	'srpg_settingsPage_collection_section' 
    );
    
    add_settings_field( 
    	'collection_photo_limit', 
    	__( 'Max Number of Photos Shown for Each Gallery', $this->plugin_name ), 
    	array( $this, 'render_field_collection_photo_limit' ), 
    	'settingsPage', 
    	'srpg_settingsPage_collection_section' 
    );

    add_settings_field( 
    	'collection_render_caption', 
    	__( 'Photo Captions on Collection & Archive Pages', $this->plugin_name ), 
    	array( $this, 'render_field_collection_caption' ), 
    	'settingsPage', 
    	'srpg_settingsPage_collection_section' 
    ); 
    
    add_settings_field( 
    	'collection_render_body', 
    	__( 'Body Content on Collection & Archive Pages', $this->plugin_name ), 
    	array( $this, 'render_field_collection_body' ), 
    	'settingsPage', 
    	'srpg_settingsPage_collection_section' 
    ); 
    
    add_settings_field( 
    	'collection_link_thumbnail_to', 
    	__( 'Link Thumbnails To', $this->plugin_name ), 
    	array( $this, 'render_field_collection_link_thumbnail_to' ), 
    	'settingsPage', 
    	'srpg_settingsPage_collection_section' 
    );
    
	}
	

  /**
	 * Render the field for medium breakpoint.
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_medium_breakpoint() { 
  
    $value = get_option( 'srpg_settings_medium_breakpoint', 768 );
    ?>
    <input size="8" class="numeric" name="srpg_settings_medium_breakpoint" type="text" placeholder="Enter Value" value="<?php echo $value; ?>">
    <br><small><em>This is the minimum browser width considered to fall under the "medium" layout.<br>Screen/browser widths below this breakpoint will see the "small" layout.</em></small>
  <?php
  }
  

  /**
	 * Render the field for large breakpoint.
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_large_breakpoint() { 
  
    $value = get_option( 'srpg_settings_large_breakpoint', 1030 );
    ?>
    <input size="8" class="numeric" name="srpg_settings_large_breakpoint" type="text" placeholder="Enter Value" value="<?php echo $value; ?>">
    <br><small><em>This is the minimum browser width considered to fall under the "large" layout.</em></small>
  <?php
  }
  

  /**
	 * Render the field for minimum distance between photos.
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_photo_padding() { 
  
    $value = get_option( 'srpg_settings_photo_padding', 5 );
    ?>
    <input size="5" class="numeric" name="srpg_settings_photo_padding" type="text" placeholder="Enter Value" value="<?php echo $value; ?>">
    <br><small><em>
    This is the minimum padding/spacing between individual photos inside a gallery/collection.<br>
    You can override this in your theme stylesheet using the .photo-thumb selector.
    </em></small>
  <?php
  }
  

	/**
	 * Callback for collection settings section.
	 * 
	 * @access public
	 * @return void
	 */
	public function settings_section_breakpoint_callback() {   
    echo __( 'These settings control the breakpoints between what is considered "small," "medium," and "large" below.<br>
    For example, you may choose to have mobile sizes correspond to "small," tablet sizes to "medium," and desktop sizes to "large."', $this->plugin_name );  
  }
  

	/**
	 * Render the field for Number of Columns (Small).
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_gallery_cols_small() { 
  
    $value = get_option( 'srpg_settings_gallery_cols_small', 1 );
    ?>
    <select id="gallery_cols_small" name="srpg_settings_gallery_cols_small">
    	<option value='1' <?php selected( $value, 1 ); ?>>1 Column</option>
    	<option value='2' <?php selected( $value, 2 ); ?>>2 Columns</option>
    	<option value='3' <?php selected( $value, 3 ); ?>>3 Columns</option>
    	<option value='4' <?php selected( $value, 4 ); ?>>4 Columns</option>
    	<option value='5' <?php selected( $value, 5 ); ?>>5 Columns</option>
    	<option value='6' <?php selected( $value, 6 ); ?>>6 Columns</option>
    	<option value='7' <?php selected( $value, 7 ); ?>>7 Columns</option>
    	<option value='8' <?php selected( $value, 8 ); ?>>8 Columns</option>
    	<option value='9' <?php selected( $value, 9 ); ?>>9 Columns</option>
    	<option value='10' <?php selected( $value, 10 ); ?>>10 Columns</option>

    	
    </select>
    <br><small><em>"Small" in this case refers to browser widths less than the "medium" breakpoint set above.</em></small>
  <?php
  }
  
  /**
	 * Render the field for Number of Columns (Medium).
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_gallery_cols_medium() { 
  
    $value = get_option( 'srpg_settings_gallery_cols_medium', 3 );
    ?>
    <select id="gallery_cols_medium" name="srpg_settings_gallery_cols_medium">
    	<option value='1' <?php selected( $value, 1 ); ?>>1 Column</option>
    	<option value='2' <?php selected( $value, 2 ); ?>>2 Columns</option>
    	<option value='3' <?php selected( $value, 3 ); ?>>3 Columns</option>
    	<option value='4' <?php selected( $value, 4 ); ?>>4 Columns</option>
    	<option value='5' <?php selected( $value, 5 ); ?>>5 Columns</option>
    	<option value='6' <?php selected( $value, 6 ); ?>>6 Columns</option>
    	<option value='7' <?php selected( $value, 7 ); ?>>7 Columns</option>
    	<option value='8' <?php selected( $value, 8 ); ?>>8 Columns</option>
    	<option value='9' <?php selected( $value, 9 ); ?>>9 Columns</option>
    	<option value='10' <?php selected( $value, 10 ); ?>>10 Columns</option>
    </select>
    <br><small><em>"Medium" in this case refers to browser widths between the "medium" and "large" breakpoints set above.</em></small>
  <?php
  }
  
  /**
	 * Render the field for Number of Columns (Large).
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_gallery_cols_large() { 
  
    $value = get_option( 'srpg_settings_gallery_cols_large', 5 );
    ?>
    <select id="gallery_cols_large" name="srpg_settings_gallery_cols_large">
    	<option value='1' <?php selected( $value, 1 ); ?>>1 Column</option>
    	<option value='2' <?php selected( $value, 2 ); ?>>2 Columns</option>
    	<option value='3' <?php selected( $value, 3 ); ?>>3 Columns</option>
    	<option value='4' <?php selected( $value, 4 ); ?>>4 Columns</option>
    	<option value='5' <?php selected( $value, 5 ); ?>>5 Columns</option>
    	<option value='6' <?php selected( $value, 6 ); ?>>6 Columns</option>
    	<option value='7' <?php selected( $value, 7 ); ?>>7 Columns</option>
    	<option value='8' <?php selected( $value, 8 ); ?>>8 Columns</option>
    	<option value='9' <?php selected( $value, 9 ); ?>>9 Columns</option>
    	<option value='10' <?php selected( $value, 10 ); ?>>10 Columns</option>
    </select>
    <br><small><em>"Large" in this case refers to browser widths greater than the "large" breakpoint set above.</em></small>
  <?php
  }
	
	
  /**
	 * Render the checkbox to show captions beneath photos for galleries
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_gallery_caption() { 
  
    $value = get_option( 'srpg_settings_gallery_render_caption', false );
    ?>
    <input type="checkbox" name="srpg_settings_gallery_render_caption" id="gallery_render_caption" <?php checked( $value, 'on' ); ?>>
    <label for="gallery_render_caption">Show Captions</label>
  <?php
  }
  
	/**
	 * Render the field for Gallery - Link Thumbnail To.
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_gallery_link_thumbnail_to() { 
  
    $value = get_option( 'srpg_settings_gallery_link_thumbnail_to', 'lightbox' );
    ?>
    <select id="gallery_link_thumbnail_to" name="srpg_settings_gallery_link_thumbnail_to">
    	<option value='lightbox' <?php selected( $value, 'lightbox' ); ?>>Lightbox View of Full Image</option>
    	<option value='raw' <?php selected( $value, 'raw' ); ?>>Raw Media File</option>
    	<option value='attachment' <?php selected( $value, 'attachment' ); ?>>Attachment Page</option>
    	<option value='none' <?php selected( $value, 'none' ); ?>>Nothing</option>
    </select>
    <br><small><em>This determines what happens when a user clicks a thumbnail image in a gallery.</em></small>
  <?php
  }
  
	/**
	 * Callback for gallery settings section.
	 * 
	 * @access public
	 * @return void
	 */
	public function settings_section_gallery_callback() {   
    echo __( 'These settings affect how your photo galleries are displayed. <br>Note that the terms "small," "medium," and "large" here refer to the breakpoints set above.', $this->plugin_name );
  }
  

  /**
	 * Render the field for Type of Display.
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_collection_display_type() { 
  
    $value = get_option( 'srpg_settings_collection_display_type', 'single' );
    ?>
    <select id="collection_display_type" name="srpg_settings_collection_display_type">
    	<option value='single' <?php selected( $value, 'single' ); ?>>Single Photo Thumbnail Linked to Gallery</option>
    	<option value='multi' <?php selected( $value, 'multi' ); ?>>Multiple Photos From Gallery</option>
    	<option value='none' <?php selected( $value, 'none' ); ?>>No Gallery Preview</option>
    </select>
    <br><small><em>
    This determines how you would like the collection/archive pages to list individual galleries.<br>
    While the "single" option is the default, you may choose to display a selection of multiple photos<br>
    from each gallery, or you may also choose not to display a preview (e.g., to show gallery titles only).
    </em></small>
  <?php
  }
  
  
	/**
	 * Render the field for Number of Columns (Small).
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_collection_cols_small() { 
  
    $display_type = get_option( 'srpg_settings_collection_display_type', 'single' );
    $value = get_option( 'srpg_settings_collection_cols_small', 1 );
    ?>
    <select <?php echo ( $display_type != 'multi' ? 'DISABLED' : ''); ?> class="hide-unless-display-type-multi"
      id="collection_cols_small" name="srpg_settings_collection_cols_small">
    	<option value='1' <?php selected( $value, 1 ); ?>>1 Column</option>
    	<option value='2' <?php selected( $value, 2 ); ?>>2 Columns</option>
    	<option value='3' <?php selected( $value, 3 ); ?>>3 Columns</option>
    	<option value='4' <?php selected( $value, 4 ); ?>>4 Columns</option>
    	<option value='5' <?php selected( $value, 5 ); ?>>5 Columns</option>
    	<option value='6' <?php selected( $value, 6 ); ?>>6 Columns</option>
    	<option value='7' <?php selected( $value, 7 ); ?>>7 Columns</option>
    	<option value='8' <?php selected( $value, 8 ); ?>>8 Columns</option>
    	<option value='9' <?php selected( $value, 9 ); ?>>9 Columns</option>
    	<option value='10' <?php selected( $value, 10 ); ?>>10 Columns</option>
    </select>
    <br><small><em>"Small" in this case refers to browser widths less than the "medium" breakpoint set above.</em></small>
  <?php
  }
  
  /**
	 * Render the field for Number of Columns (Medium).
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_collection_cols_medium() { 
  
    $display_type = get_option( 'srpg_settings_collection_display_type', 'single' );
    $value = get_option( 'srpg_settings_collection_cols_medium', 3 );
    ?>
    <select <?php echo ( $display_type != 'multi' ? 'DISABLED' : ''); ?> class="hide-unless-display-type-multi"
      id="collection_cols_medium" name="srpg_settings_collection_cols_medium">
    	<option value='1' <?php selected( $value, 1 ); ?>>1 Column</option>
    	<option value='2' <?php selected( $value, 2 ); ?>>2 Columns</option>
    	<option value='3' <?php selected( $value, 3 ); ?>>3 Columns</option>
    	<option value='4' <?php selected( $value, 4 ); ?>>4 Columns</option>
    	<option value='5' <?php selected( $value, 5 ); ?>>5 Columns</option>
    	<option value='6' <?php selected( $value, 6 ); ?>>6 Columns</option>
    	<option value='7' <?php selected( $value, 7 ); ?>>7 Columns</option>
    	<option value='8' <?php selected( $value, 8 ); ?>>8 Columns</option>
    	<option value='9' <?php selected( $value, 9 ); ?>>9 Columns</option>
    	<option value='10' <?php selected( $value, 10 ); ?>>10 Columns</option>
    </select>
    <br><small><em>"Medium" in this case refers to browser widths between the "medium" and "large" breakpoints set above.</em></small>
  <?php
  }
  
  /**
	 * Render the field for Number of Columns (Large).
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_collection_cols_large() { 
  	
    $display_type = get_option( 'srpg_settings_collection_display_type', 'single' );
    $value = get_option( 'srpg_settings_collection_cols_large', 5 );
    ?>
    <select <?php echo ( $display_type != 'multi' ? 'DISABLED' : ''); ?> class="hide-unless-display-type-multi" 
      id="collection_cols_large" name="srpg_settings_collection_cols_large">
    	<option value='1' <?php selected( $value, 1 ); ?>>1 Column</option>
    	<option value='2' <?php selected( $value, 2 ); ?>>2 Columns</option>
    	<option value='3' <?php selected( $value, 3 ); ?>>3 Columns</option>
    	<option value='4' <?php selected( $value, 4 ); ?>>4 Columns</option>
    	<option value='5' <?php selected( $value, 5 ); ?>>5 Columns</option>
    	<option value='6' <?php selected( $value, 6 ); ?>>6 Columns</option>
    	<option value='7' <?php selected( $value, 7 ); ?>>7 Columns</option>
    	<option value='8' <?php selected( $value, 8 ); ?>>8 Columns</option>
    	<option value='9' <?php selected( $value, 9 ); ?>>9 Columns</option>
    	<option value='10' <?php selected( $value, 10 ); ?>>10 Columns</option>
    </select>
    <br><small><em>"Large" in this case refers to browser widths greater than the "large" breakpoint set above.</em></small>
  <?php
  }
  
  
  /**
	 * Render the field for limit of photos shown in a collection.
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_collection_photo_limit() { 
    
    $display_type = get_option( 'srpg_settings_collection_display_type', 'single' );
    $value = get_option( 'srpg_settings_collection_photo_limit', -1 );
    ?>
    <input <?php echo ( $display_type != 'multi' ? 'DISABLED' : ''); ?> class="hide-unless-display-type-multi numeric" 
      size="5" type="text" id="collection_photo_limit" name="srpg_settings_collection_photo_limit" value="<?php echo $value; ?>">
    <br><small><em>
    Set to -1 for no limit; if a limit is set, the user will need to click through to view the full gallery.
    </em></small>
  <?php
  }

  /**
	 * Render the checkbox to show captions beneath photos for collections/archives
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_collection_caption() { 
  
    $display_type = get_option( 'srpg_settings_collection_display_type', 'single' );
    $value = get_option( 'srpg_settings_collection_render_caption', false );
    ?>
    <input <?php echo ( $display_type != 'multi' ? 'DISABLED' : ''); ?> class="hide-unless-display-type-multi"
      type="checkbox" name="srpg_settings_collection_render_caption" id="collection_render_caption" <?php checked( $value, 'on' ); ?>>
    <label for="collection_render_caption">Show Captions</label>
  <?php
  }
  
  /**
	 * Render the checkbox to show captions beneath photos for collections/archives
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_collection_body() { 
  
    $display_type = get_option( 'srpg_settings_collection_display_type', 'single' );
    $value = get_option( 'srpg_settings_collection_render_body', false );
    ?>
    <input <?php echo ( $display_type != 'multi' ? 'DISABLED' : ''); ?> class="hide-unless-display-type-multi"
      type="checkbox" name="srpg_settings_collection_render_body" id="collection_render_body" <?php checked( $value, 'on' ); ?>>
    <label for="collection_render_body">Show Body Content</label>
    <br><small><em>"Body Content" refers to what (if anything) appears just above the photos on individual gallery pages.</em></small>
  <?php
  }
  
	/**
	 * Render the field for Collection - Link Thumbnail To.
	 * 
	 * @access public
	 * @return void
	 */
	public function render_field_collection_link_thumbnail_to() { 
  
    $display_type = get_option( 'srpg_settings_collection_display_type', 'single' );
    $value = get_option( 'srpg_settings_collection_link_thumbnail_to', 'gallery' );
    ?>
    <select <?php echo ( $display_type != 'multi' ? 'DISABLED' : ''); ?> class="hide-unless-display-type-multi" 
      id="collection_link_thumbnail_to" name="srpg_settings_collection_link_thumbnail_to">
      <option value='gallery' <?php selected( $value, 'gallery' ); ?>>Gallery to Which the Photo Belongs</option>
    	<option value='lightbox' <?php selected( $value, 'lightbox' ); ?>>Lightbox View of Full Image</option>
    	<option value='raw' <?php selected( $value, 'raw' ); ?>>Raw Media File</option>
    	<option value='attachment' <?php selected( $value, 'attachment' ); ?>>Attachment Page</option>
    	<option value='none' <?php selected( $value, 'none' ); ?>>Nothing</option>
    </select>
    <br><small><em>This determines what happens when a user clicks a thumbnail image in a collection/archive.</em></small>
  <?php
  }
  
	
	/**
	 * Callback for collection settings section.
	 * 
	 * @access public
	 * @return void
	 */
	public function settings_section_collection_callback() {   
    echo __( 'These settings affect how <em>collections</em> of galleries are displayed, as well as the main archive (which lists all of your photo galleries).', $this->plugin_name );  
  }
  


	
	/**
	 * Render a plugin settings page.
	 * 
	 * @access public
	 * @return void
	 */
	public function options_page() { 
  
    ?>
    <div class="wrap">
      <form action='options.php' method='post'>
      	
      	<h1>Simple Responsive Photo Galleries</h1>
      	
      	<?php
      	settings_fields( 'settingsPage' );
      	do_settings_sections( 'settingsPage' );
      	submit_button();
      	?>
      	
      </form>
    </div>
    <?php  
  }

}
