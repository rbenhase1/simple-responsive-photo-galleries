(function( $ ) {
	'use strict';

  $(document).ready(function(){
    
    // Function to select a single photo in the list
    function selectPhoto( elem ) {      
      if (elem.hasClass("selected")) {
        elem.removeClass("selected");
      } else {
        $(".selected").removeClass("selected");
        elem.addClass("selected");
      }
    }
    
    // Initialize jQuery UI sortable
    $("#srpg_photos_container").sortable( {
      start: function(event,ui) {
        if (ui.item.hasClass("selected")) {
          ui.item.removeClass("selected");
        }
        selectPhoto(ui.item);
      }
    });
    
    // Edit photo dialog
    $(document).on("click", ".edit-photo", function(e) {
      e.preventDefault();
      e.stopPropagation();
      
      var parentIndex = $(this).parent().index();
      var mediumUrl = $(this).data("medium-url");      
      var attachment = $(this).data("attachment");   
      var editDialog = $("<div></div>").attr("id","edit-photo-dialog");      
      var mediumPhoto = $("<img>").attr("src",mediumUrl).addClass("medium-preview");      
      var clearfix = $("<div></div>").addClass("clearfix");
      
      editDialog.append(mediumPhoto);
      editDialog.append(clearfix.clone());
      
      var label = $("<label></label>").text("Photo Caption:");
      
      editDialog.append(label);
      editDialog.append(clearfix.clone());
      
      var input = $("<input>").attr("id","edit-photo-caption");
      input.attr("placeholder","Enter caption here...");
      
      editDialog.append(input);
      editDialog.append(clearfix.clone());
      
      $.post(
        ajaxurl,
        { 
          action: 'srpg_get_caption',
          attachment: attachment       
        },
        function(response) {
          input.val(response.caption);
          console.log(response);
        }
      );    
      
      var updateButton = $("<a></a>").attr("id","dialog-update-photo");
      updateButton.attr("data-parent",parentIndex);
      updateButton.addClass("button-primary").text("Update Photo");
      
      var deleteButton = $("<a></a>").attr("id","dialog-delete-photo");
      deleteButton.attr("data-parent",parentIndex);
      deleteButton.addClass("button-secondary").text("Delete Photo");
      
      editDialog.append(updateButton);
      editDialog.append(deleteButton);
      editDialog.append(clearfix.clone());
      
      // Render the dialog
      editDialog.dialog({
			  title: "Photo " + ( parentIndex + 1 ) + " of " + $("#srpg_photos_container li").length,
			  width: 500,
			  modal: true,
			  position: { my: "center center", at: "center center", of: window}, 				  
		  });

    });
    
    // Save updated photo caption
    $(document).on("click", "#dialog-update-photo", function(e) {
      e.preventDefault();
      
      var parent = $(this).data("parent");
      var attachment = $(".photo-preview:eq(" + parent + ")").data("attachment");
      
      $.post(
        ajaxurl,
        { 
          action: 'srpg_update_caption',
          attachment: attachment,
          caption: $("#edit-photo-caption").val()          
        },
        function(response) {
          $("#edit-photo-dialog").dialog('destroy').remove();
        }
      );      
    });
    
    // Delete a photo
    $(document).on("click", "#dialog-delete-photo", function(e) {
      e.preventDefault();
      
      var parent = $(this).data("parent");
      $(".photo-preview:eq(" + parent + ")").remove();
      
      $("#edit-photo-dialog").dialog('destroy').remove();      
    });
    
    
    // Allow photos to be selected (toggle class)
    $(document).on("click", ".photo-preview", function(){
      selectPhoto($(this));
    });

    // Instantiates the variable that holds the media library frame.
    var meta_photo_frame;

    // Runs when the image button is clicked.
    $('#meta-photo-button').click(function(e){

      // Prevents the default action from occuring.
      e.preventDefault();

      // If the frame already exists, re-open it.
      if ( meta_photo_frame ) {
         meta_photo_frame.open();
          return;
      }

      // Sets up the media library frame
      meta_photo_frame = wp.media.frames.meta_photo_frame = wp.media({
          title: srpg_photos.title,
          button: { text:  srpg_photos.button },
          library: { type: 'image/jpeg,image/png' },
          multiple: true
      });
      
      // Filter by file type
      if (meta_photo_frame.uploader) {
        meta_photo_frame.uploader.options.uploader.params.allowed_mime_types = 'image/jpeg,image/png';
        meta_photo_frame.uploader.options.uploader.params.filetype_not_allowed_message = "You can only upload or select photos (JPG or PNG format).";
      }

      // Runs when a file is selected.
      meta_photo_frame.on('select', function(){                  
        
        var selection = meta_photo_frame.state().get('selection');
        
        selection.map( function( attachment ) {

          var media_attachment = attachment.toJSON();

          var item = $("<li></li>").addClass("photo-preview");
          item.attr("data-attachment",media_attachment.id);
          
          var image = $("<img>").attr("src", media_attachment.sizes.thumbnail.url);
          
          var input = $("<input>").attr("type","hidden").attr("name","srpg_photos[]");
          
          var editBtn = $("<a></a>").addClass("edit-photo").html('<span class="dashicons dashicons-edit"></span>');
          editBtn.attr("data-medium-url",media_attachment.sizes.medium.url);
          editBtn.attr("data-attachment",media_attachment.id);
          editBtn.attr("data-caption","");
          
          
          image.data("attachment",media_attachment.id);
          input.val(media_attachment.id);
          
          item.append(image).append(input).append(editBtn);          
          $('#srpg_photos_container').append(item);            
          
        });

        return false;
      });
      
      // Opens the media library frame.
      meta_photo_frame.open();
    });
  });

})( jQuery );