<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://ryanbenhase.com
 * @since             1.0.0
 * @package           Simple_Responsive_Photo_Galleries
 *
 * @wordpress-plugin
 * Plugin Name:       Simple Responsive Photo Galleries
 * Plugin URI:        http://2060digital.com
 * Description:       Easily create and manage responsive photo galleries on your Wordpress site.
 * Version:           1.0.0
 * Author:            Ryan Benhase
 * Author URI:        http://ryanbenhase.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       simple-responsive-photo-galleries
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-simple-responsive-photo-galleries-activator.php
 */
function activate_simple_responsive_photo_galleries() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-simple-responsive-photo-galleries-activator.php';
	Simple_Responsive_Photo_Galleries_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-simple-responsive-photo-galleries-deactivator.php
 */
function deactivate_simple_responsive_photo_galleries() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-simple-responsive-photo-galleries-deactivator.php';
	Simple_Responsive_Photo_Galleries_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_simple_responsive_photo_galleries' );
register_deactivation_hook( __FILE__, 'deactivate_simple_responsive_photo_galleries' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-simple-responsive-photo-galleries.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_simple_responsive_photo_galleries() {

	$plugin = new Simple_Responsive_Photo_Galleries();
	$plugin->run();

}
run_simple_responsive_photo_galleries();
